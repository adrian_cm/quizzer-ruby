# coding: utf-8
#lib = File.expand_path('../lib', __FILE__)
#$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
#require 'Quizzer/Ruby/version'

Gem::Specification.new do |spec|
  spec.name          = "quizzer"
  spec.version       = "0.1.9"
  spec.authors       = ["Adrian Cepillo"]
  spec.email         = ["adrian.cepillo@gmail.com"]
  spec.summary       = %q{Quizzer package evaluates exams, answers and generates scores.}
  spec.description   = %q{Quizzer package evaluates exams, answers and generates scores.}
  spec.homepage      = "http://bitbucket.org/adrian_cm/quizzer-ruby"
  spec.license       = "MIT"

  spec.files         = ["lib/quizzer.rb"] #`git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake", "~>0"
end
