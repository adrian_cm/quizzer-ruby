#! /usr/bin/ruby

require 'quizzer'
require 'optparse'

options={}

QUIZZER_VERSION = '0.1.8'

def generate(quizz, assessment, output = nil)
  q = Quizzer.new(quizz, assessment)
  out = q.generate_scores(output)
  if out === true
    puts "Success: Scores file create on #{output}.\n"
  else
    puts out
  end
end


def manifest(path)

  outputs = Quizzer.manifest(path)

  outputs.each_index do |index|
    if outputs[index].last
      puts "Test #{index+1}:  Passed"
    else
      puts "Test #{index+1}:  Not passed"
    end
  end
end

begin

  opts = OptionParser.new

  opts.banner = "Usage: quizzer-cli.rb [options]"

  opts.on("-g", "--generate <QUIZZ>,<ASSESSMENT>[,<OUTPUT_PATH>]", Array,
          "Generate scores from local or remote quizz.json and assessment.json") do |g|
    options[:generate] = g
  end

  opts.on("-m", "--manifest <MANIFEST>", "Input a local or remote manifest.json file (see README file)") do |m|
    options[:manifest] = m
  end

  opts.on_tail("-h", "--help", "Show this help message") do
    puts opts
  end

  opts.on_tail("-v", "--version", "Show quizzer gem version") do
    puts QUIZZER_VERSION
  end

  opts.parse!


  if options[:generate] && options[:manifest]

    raise "Error: Two incompatibles options"

  elsif options[:generate]

    generate(options[:generate][0], options[:generate][1], options[:generate][2])

  elsif options[:manifest]

    manifest(options[:manifest])

  end

rescue Exception => e

  puts opts
  puts e.message
  #puts e.backtrace
# ignored
end