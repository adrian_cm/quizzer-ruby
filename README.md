# Quizzer::Ruby

Quizzer package evaluates exams, answers and generates scores.

## Installation

Add this line to your application's Gemfile:

    gem 'quizzer'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install quizzer

## Usage

Add this line to include the gem:

    require 'quizzer'
    
And then create a quizzer:

    quizzer = Quizzer.new(quizz_path, assessment_path)
    
Or create from a manifest file (could be a local or remote file):

    array = Quizzer.manifest(path) # Array of arrays which contents [Quizzer, test_passed?]
    
Finally generate your scores:

    quizzer.generate_scores(optional_output_path) #if there is not output_path, it returns a json string

## Contributing

1. Fork it ( https://bitbucket.org/adrian_cm/quizzer-ruby )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
