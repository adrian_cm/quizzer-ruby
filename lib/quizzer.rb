require 'json'
require 'open-uri'

class Quizzer
  attr_accessor :quizz, :assessment, :scores

  def initialize(quizz, assessment)
    raise "Obligatory quizz and assessment input" unless quizz || assessment

    @quizz = Quizz.new(JSON.parse(Quizzer.get_file(quizz)))
    @assessment = Assessment.new(JSON.parse(Quizzer.get_file(assessment)))
    @scores = Scores.new(@quizz, @assessment)
  end

  def self.manifest(file)
    out = Array.new
    m = JSON.parse(get_file(file))
    m["tests"].each do |t|
      out.push self.test(t["quizz"], t["assessment"], t["scores"])
    end
    out
  end

  def self.test(quizz, assessment, scores)
    q = Quizzer.new(quizz, assessment)
    s_test = JSON.parse get_file(scores)
    if q.scores.test(s_test)
      return [q, true]
    else
      return [q, false]
    end
  end

  def generate_scores(output = nil)
    if output
      File.open(output,"w") do |f|
        f.write(@scores.serialize)
      end
      true
    else
      @scores.serialize
    end
  end

  def self.get_file(path)
    /^http:\/\//.match(path) ? open(path).read : IO.read(path)
  end

end

class Quizz
  attr_accessor :questions

  def initialize(quizz)
    @questions = Hash.new
    quizz["questions"].each do |q|
      case q["type"]
        when 'truefalse' then
          @questions[q["id"]] = TrueFalse.new(q["type"], q["id"], q["questionText"], q["correct"],
                                            { value_ok: q["valueOK"],
                                              value_failed: q["valueFailed"],
                                              feedback: q["feedback"] })
        when 'multichoice' then
          alt = Hash.new
          q["alternatives"].each { |a| alt[a["code"]] = Alternative.new(a["text"], a["code"], a["value"]) }
          @questions[q["id"]] = Multichoice.new(q["type"], q["id"], q["questionText"], alt)
        else
          raise "Unknown type of question"
      end
    end
  end
end

class Question
  attr_accessor :type, :id, :question_text
  def initialize(type, id, question_text)
    @type = type
    @id = id
    @question_text = question_text
  end
end

class TrueFalse < Question
  attr_accessor :correct, :value_ok, :value_failed, :feedback
  def initialize(type, id, question_text, correct, prop = Hash.new)
    super(type, id, question_text)
    @correct = correct
    @value_ok = prop["value_ok"] || 1
    @value_failed = prop["value_failed"] || -0.25
    @feedback = prop["feedback"] || String.new
  end
end

class Multichoice < Question
  attr_accessor :alternatives
  def initialize(type, id, question_text, alternatives = Hash.new)
    super(type, id, question_text)
    @alternatives = alternatives
  end
end

class Alternative
  attr_accessor :text, :code, :value
  def initialize(text, code, value)
    @text = text
    @code = code
    @value = value
  end
end

class Assessment
  attr_accessor :items
  def initialize(assessment)
    @items = Hash.new
    assessment["items"].each do |i|
      answers = Hash.new
      i["answers"].each { |a| answers[a["question"]] = Answer.new(a["question"], a["value"]) }
      @items[i["studentId"]] = Item.new(i["studentId"], answers)
    end
  end
end

class Item
  attr_accessor :student_id, :answers
  def initialize(student_id, answers = Hash.new)
    @student_id = student_id
    @answers = answers
  end
end

class Answer
  attr_accessor :question, :value
  def initialize(question, value)
    @question = question
    @value = value
  end
end

class Scores
  attr_accessor :scores
  def initialize(quiz, assesment)
    @scores = Hash.new
    assesment.items.each do |i_id, i|
      s = Score.new(i_id)
      quiz.questions.each do |q_id, q|
        case q.type
          when 'truefalse' then
            if q.correct == i.answers[q_id].value
              s.value += q.value_ok
            else
              s.value += q.value_failed
            end
          when 'multichoice' then
            s.value += q.alternatives[i.answers[q_id].value].value
          else
            raise 'Incomplete question'
        end
      end
      @scores[i_id] = s
    end
  end

  def test(another)
    s_test = Hash.new
    another["scores"].each do |s|
      s_test[s["studentId"]] = Score.new(s["studentId"], s["value"])
    end

    @scores == s_test
  end

  def serialize()
    output = { "scores" => Array.new }
    @scores.each do |s_id, s|
      output["scores"].push({ "studentId" => s_id, "value" => s.value })
    end
    JSON.pretty_generate(output)
  end
end

class Score
  attr_accessor :student_id, :value
  def initialize(student_id, value = 0)
    @student_id = student_id
    @value = value
  end

  def == (score)
    @student_id == score.student_id && @value == score.value
  end
end

